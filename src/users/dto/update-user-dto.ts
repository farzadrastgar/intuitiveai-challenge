export class UpdateUserDto {
  email?: string;
  password?: string;
  refreshToken?: string;
}
